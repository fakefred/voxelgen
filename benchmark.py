from voxelgen import voxelgen, visualize
from probabilities import PROBS
from datetime import datetime

"""
Benchmarking for voxelgen, namely a speed test.
Runs voxelgen from scale=1 to 49; 
records how long it takes to generate and visualize the world.
"""

for scale in range(1, 51, 2):
    # scale = 1, 3, 5, ..., 49
    begin_t = datetime.now()
    world = voxelgen(scale, PROBS)
    gen_t = datetime.now()
    # visualize(world)
    # vis_t = datetime.now()

    gen_dt = gen_t - begin_t
    # vis_dt = vis_t - gen_t

    # print(f'scale={scale}: gen {gen_dt}')
    print(f'{scale}, {gen_dt.seconds * 1_000_000 + gen_dt.microseconds}')
