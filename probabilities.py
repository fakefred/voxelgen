"""
0 air
1 rock
2 dirt
3 grass
254 water (not registered here)
255 undefined (neither)
"""
PROBS = {
    0: [
        [0.9, 0.05, 0.05, 0],
        [1, 0, 0, 0],
        [0.8, 0.2, 0, 0]
    ],
    # suppose we know on (1, 1, 1) there is a lone rock voxel:
    1: [
        # we can infer what its adjacent blocks to six directions
        # are likely to be with this probability table
        # left, right, in, out
        # the four floats are the likeliness of
        # air, rock, dirt, grass, and water to their side
        # they add up to 1.
        [0.05, 0.9, 0.05, 0],
        # up:
        # what's more likely to be on top of a rock is another rock.
        [0.15, 0.6, 0.25, 0],
        # down:
        # we may have air below rocks; they are caves.
        [0.2, 0.8, 0, 0]
    ],
    2: [
        [0.05, 0.05, 0.7, 0.2],
        [0.15, 0, 0.6, 0.25],
        [0, 0.4, 0.6, 0]
    ],
    3: [
        [0.2, 0, 0.3, 0.5],
        [1, 0, 0, 0],
        [0, 0, 1, 0]
    ]
}
